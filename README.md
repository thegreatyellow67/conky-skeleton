## Conky Skeleton (version 1.3 - 07/2019)

Conky Skeleton is a collection of themes for [conky](https://github.com/brndnmtthws/conky) that are a mix of [zagortenay333 / Harmattan](https://github.com/zagortenay333/Harmattan) (only for the weather script section and powered by [OpenWeatherMap](http://openweathermap.org/)) and [addy-dclxvi / Informant](https://github.com/addy-dclxvi/conky-theme-collections)

---

* [Compatibility](#compatibility)
* [Installation](#installation)
* [Unistallation](#uninstallation)
* [Use](#use)
* [API-Key](#api-key)
* [City](#city)
* [Units](#units)
* [Language](#language)
* [Themes](#themes)
* [Issues](#issues)
* [Credits](#credits)
* [Changelog](#changelog)
* [Previews](#previews)
* [Wallpapers](#wallpapers)

---

### Compatibility

* The latest version 1.3 of these themes supports conky `1.10.x`.

* These scripts are tested in Debian stretch 9.6 (Conky version 1.10.6) with a monitor resolution of 1920 x 1080.

* From distro to distro some adjustments may be necessary.

* Even for different hardware, adjustments maybe necessary (CPU, WiFi, hard drives, etc.). See section [Issues](#issues).

* At the moment, the GPU graphic section is only for NVIDIA cards (requires separate command **nvidia-settings**).

---

### Installation

* Install **conky**, **curl** and **jq**.

* Make sure you have the **Roboto** font installed (https://www.fontsquirrel.com/fonts/roboto). In debian based systems (Ubuntu, Mint, Debian, etc.) you can install it with the following command: **sudo apt install fonts-roboto-unhinted**.

* Download a compressed archive of this repository (clicking on the **Download** button) and decompress it in a temporary folder: <img src="https://i.imgur.com/bxj7lM9.png">

* Alternately, clone it with the command: `git clone https://gitlab.com/thegreatyellow67/conky-skeleton.git`

* Register a private API key on [OpenWeatherMap](http://openweathermap.org/) to get weather data.

* [Find your city ID](http://bulk.openweathermap.org/sample/city.list.json.gz); it's a compressed file list, decompress and look for. Alternately, there is a city.list.json file in the **.docs** Skeleton folder.

* In a terminal run script **skeleton_setup** inside the temporary skeleton folder and follow the instructions **(first, remember to get the Api Key and City ID - see sections below)**.

---

### Uninstallation

* Simply run script **skeleton_uninstall** in conky-skeleton temp folder or manually remove `$HOME/.cache/skeleton-conky/` directory, `$HOME/.conky/Conky-Skeleton/` directory and `$HOME/.conky/skeleton_start`, `$HOME/.conky/skeleton_start_random` and `$HOME/.conky/skeleton_demo` files.
---

### Use

* With script **skeleton_demo** you can make a slideshow of all the skeleton themes, running in a terminal:

	`skeleton_demo [number_of_seconds]`

* With script **skeleton_start** you can run just only one theme at time, running in a terminal:

	`skeleton_start [color_theme_name]`

* The script **skeleton_start_random** chooses for you a random theme.

---

### API-Key

Register a private API key on [OpenWeatherMap](http://openweathermap.org/) to get weather data.

Use it in setup script **skeleton_setup**.
You can change later in each color theme the value of `template6` variable inside the `skeleton-$COLOR.lua` file.

---

### City ID

[Find your city ID](http://bulk.openweathermap.org/sample/city.list.json.gz).
There is also a city.list.json in the .docs Conky-Harlequin folder where you can find your city ID.

Use it in setup script **skeleton_setup**.
You can change later in each color theme the value of `template7` variable inside the `skeleton-$COLOR.lua` file.

---

### Units

The script setup **skeleton_setup** asks you to choice for the units value.
You can change later in each color theme the value of `template8` variable inside the `skeleton-$COLOR.lua` file (values are: default, metric, imperial).

---

### Language

In script setup **skeleton_setup** if you leave "local language" empty, these conky themes use your default locale.
You can change later in each color theme the value of `template9` variable inside the `skeleton-$COLOR.lua` file.

[See the list of supported languages](http://openweathermap.org/current#multi)

> **NOTE:**  
> This conky has some hardcoded text that won't get translated, but you can edit it manually.

---

### Themes

| #      | Name                                                              | #      | Name                                                              |
| :----: | ----------------------------------------------------------------- | :----: | ----------------------------------------------------------------- |
| **1**  | ![#4F4F4F](https://placehold.it/15/4F4F4F/000000?text=+) Black    | **11** | ![#16A085](https://placehold.it/15/16A085/000000?text=+) Teal     |
| **2**  | ![#607D8B](https://placehold.it/15/607D8B/000000?text=+) Bluegrey | **12** | ![#A674DE](https://placehold.it/15/A674DE/000000?text=+) Violet   |
| **3**  | ![#5294E2](https://placehold.it/15/5294E2/000000?text=+) Blue     | **13** | ![#E2B322](https://placehold.it/15/E2B322/000000?text=+) Yellow   |
| **4**  | ![#AE8E6C](https://placehold.it/15/AE8E6C/000000?text=+) Brown    |        |                                                                   |
| **5**  | ![#00BCD4](https://placehold.it/15/00BCD4/000000?text=+) Cyan     |        |                                                                   |
| **6**  | ![#87B158](https://placehold.it/15/87B158/000000?text=+) Green    |        |                                                                   |
| **7**  | ![#8E8E8E](https://placehold.it/15/8E8E8E/000000?text=+) Grey     |        |                                                                   |
| **8**  | ![#CA71DF](https://placehold.it/15/CA71DF/000000?text=+) Magenta  |        |                                                                   |
| **9**  | ![#EE923A](https://placehold.it/15/EE923A/000000?text=+) Orange   |        |                                                                   |
| **10** | ![#E25252](https://placehold.it/15/E25252/000000?text=+) Red      |        |                                                                   |

---

### Issues

**Network section**

In case the network section shouldn't be displayed, you need to find your network device name (depending on your distro, but usually with command `sudo ifconfig -a`) and replace it manually in `skeleton-$COLOR.lua` files.

> **EXAMPLE:**  
>
> if you have a **wireless device** `wlp2s0`, replace `wlp5s0` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/wlp5s0/wlp2s0/g" {} +`
>
> instead, if you have a **wired device** `eth0`, replace `eno1` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/eno1/eth0/g" {} +`
>
> Further, in section **[ CONNECTIONS ]** of each **skeleton-$COLOR.lua** file if you have a **wired LAN** manually correct the device with first substitution command (the second command find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/eno1/eth0/g" {} + doesn't replaces existing wlp5s0):
>
> `find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/wlp5s0/eth0/g" {} +`). 

**CPU section**

In **[ CPU ]** section the following row:

`${alignr}Core0 temp: ${color9}${execi 10 sensors | grep 'Core 0' | cut -c18-24} ${color1}|${color} Core1 temp: ${color9}${execi 10 sensors | grep 'Core 1' | cut -c18-24}${color}`

uses command `sensors` who gives different result depending on hardware. For example, in my first desktop PC with Gigabyte GA-Z97-D3H motherboard, the result is:

	acpitz-virtual-0
	Adapter: Virtual device
	temp1:        +27.8°C  (crit = +105.0°C)
	temp2:        +29.8°C  (crit = +105.0°C)

	coretemp-isa-0000
	Adapter: ISA adapter
	Physical id 0:  +42.0°C  (high = +80.0°C, crit = +100.0°C)
	Core 0:         +41.0°C  (high = +80.0°C, crit = +100.0°C)
	Core 1:         +43.0°C  (high = +80.0°C, crit = +100.0°C)

but, in the other desktop PC with Asus M5A78L-M/USB3 motherboard, the result is more detailed and different:

	k10temp-pci-00c3
	Adapter:  PCI adapter
	temp1:    +30.5°C  (high = +70.0°C) (crit = +83.5°C, hyst = +80.5°C)

	atk0110-acpi-0
	Adapter: ACPI interface
	Vcore Voltage:      +1.43 V  (min =  +0.80 V, max =  +1.60 V)
	+3.3V Voltage:      +3.30 V  (min =  +2.97 V, max =  +3.63 V)
	+5V Voltage:        +5.10 V  (min =  +4.50 V, max =  +5.50 V)
	+12V Voltage:      +12.11 V  (min = +10.20 V, max = +13.80 V)
	CPU Fan Speed:     2454 RPM  (min =  600 RPM, max = 7200 RPM)
	Chassis Fan Speed:    0 RPM  (min =  600 RPM, max = 7200 RPM)
	CPU Temperature:    +47.0°C  (high = +60.0°C, crit = +95.0°C)
	MB Temperature:     +38.0°C  (high = +45.0°C, crit = +75.0°C)

	fam15h_power-pci-00c4
	Adapter: PCI adapter
	power1:       37.27 W  (crit =  95.04 W)

so, in this case I've replaced the instruction above with:

`${alignr}CPU temp: ${color9}${execi 10 sensors | grep 'CPU Temp' | cut -c22-28} ${color1}|${color} CPU Fan: ${color9}${execi 10 sensors | grep 'CPU Fan' | cut -c20-27} ${color1}|${color} MB Temp: ${color9}${execi 10 sensors | grep 'MB Temp' | cut -c22-28}${color}`

giving more detailed temperature's infos. For your hardware, the fix is based on your hardware and the result of the `sensors` command. 

**Disks section**

If your linux hard disk is partitoned as my scheme (swap, root partition and home partition), you have to replace **/dev/sda2** (root partition) and **/dev/sda3** (home partition) In **[ DISKS ]** section with the following commands:

> **EXAMPLE:**  
>
> if you have a **root partition** `/dev/sdc1`, replace `/dev/sda2` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/sda2/sdc1/g" {} +`
>
> if you have a **home partition** `/dev/sdc2`, replace `/dev/sda3` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Skeleton -type f -name "skeleton*.lua" -exec sed -i'' -e "s/sda3/sdc2/g" {} +`

---

### Credits

* **zagortenay333** for Harmattan Conky (https://github.com/zagortenay333/Harmattan)
* **aresgon** for Workspace Indicator Conky
* **addy-dclxvi** for Conky Infomant (https://github.com/addy-dclxvi/conky-theme-collections)

---

### Changelog

* [11/12/2018] - Initial version 1.0.
* [11/15/2018] - Added **skeleton_uninstall** script and wallpapers.
* [11/17/2018] - Fix path for variable forecast=~/".cache/skeleton-conky/forecast.json" in script Conky-Skeleton/parse_weather.
* [11/22/2018] - Changed position for main temperature and main weather condition icon, plus added a temperature icon at the left of the main temperature.
* [11/22/2018] - The script <b>skeleton_uninstall</b> now creates a backup copy of the previous Conky-Skeleton folder, to keep possible changes made by the user.
* [11/22/2018] - Changed to version 1.1.
* [11/27/2018] - Changed to version 1.2. In meteo section modified 5 days temperatures code for resolving display issues.
* [07/15/2019] - Changed to version 1.3. Some additions and modifications.
---

### Previews

## Black
<img src="https://i.imgur.com/qvPW4OI.png" id="previews">

## Bluegrey
<img src="https://i.imgur.com/7chTKE1.png">

## Blue
<img src="https://i.imgur.com/8WUXidQ.png">

## Brown
<img src="https://i.imgur.com/pQEHQ12.png">

## Cyan
<img src="https://i.imgur.com/oruNKcf.png">

## Green
<img src="https://i.imgur.com/Ce10Iuj.png">

## Grey
<img src="https://i.imgur.com/92hZ8ti.png">

## Magenta
<img src="https://i.imgur.com/qkzpswi.png">

## Orange
<img src="https://i.imgur.com/O5oQfMM.png">

## Red
<img src="https://i.imgur.com/VOrH6D9.png">

## Teal
<img src="https://i.imgur.com/UTALNDq.png">

## Violet
<img src="https://i.imgur.com/T63VcAp.png">

## Yellow
<img src="https://i.imgur.com/wGaGs9i.png">

---

### Wallpapers

A collection of wallpapers made for Conky Skeleton.

You can download them from here: [Conky-Skeleton-Wallpapers](https://gitlab.com/thegreatyellow67/conky-skeleton-wallpapers)

## Black
<img src="https://i.imgur.com/shs0Wut.png">
<img src="https://i.imgur.com/XKTijCI.png">
<img src="https://i.imgur.com/9v5k9c8.png">
<img src="https://i.imgur.com/eVb7nxL.png">
<img src="https://i.imgur.com/00KRaof.png">

## Bluegrey
<img src="https://i.imgur.com/jO8eDVg.png">
<img src="https://i.imgur.com/ZrBkuME.png">
<img src="https://i.imgur.com/3ZMuOFn.png">
<img src="https://i.imgur.com/I1TNDyf.png">
<img src="https://i.imgur.com/tVP9AWZ.png">

## Blue
<img src="https://i.imgur.com/MT3TVAR.png">
<img src="https://i.imgur.com/iyLBpxw.png">
<img src="https://i.imgur.com/lGSCoAZ.png">
<img src="https://i.imgur.com/IU8ZgLm.png">
<img src="https://i.imgur.com/GG5qZsB.png">

## Brown
<img src="https://i.imgur.com/GIxeOoQ.png">
<img src="https://i.imgur.com/FW0RU0g.png">
<img src="https://i.imgur.com/xbsntH8.png">
<img src="https://i.imgur.com/RLTUdLz.png">
<img src="https://i.imgur.com/86Iyv94.png">

## Cyan
<img src="https://i.imgur.com/B7eEnwW.png">
<img src="https://i.imgur.com/exYAVPK.png">
<img src="https://i.imgur.com/LA9GUEM.png">
<img src="https://i.imgur.com/LIcEgvN.png">
<img src="https://i.imgur.com/p11sBbI.png">

## Green
<img src="https://i.imgur.com/PznxFxs.png">
<img src="https://i.imgur.com/ZEVVKCX.png">
<img src="https://i.imgur.com/ikLgXaY.png">
<img src="https://i.imgur.com/UQelm83.png">
<img src="https://i.imgur.com/1FD7gqJ.png">

## Grey
<img src="https://i.imgur.com/h6fd2JG.png">
<img src="https://i.imgur.com/cc3mL1K.png">
<img src="https://i.imgur.com/pqPxgnK.png">
<img src="https://i.imgur.com/3xBu0fW.png">
<img src="https://i.imgur.com/Ap3qQnU.png">

## Magenta
<img src="https://i.imgur.com/TRKFm1Z.png">
<img src="https://i.imgur.com/hoIhHsM.png">
<img src="https://i.imgur.com/s9PPR4z.png">
<img src="https://i.imgur.com/lUEGGtk.png">
<img src="https://i.imgur.com/ABNozid.png">

## Orange
<img src="https://i.imgur.com/mxFd83u.png">
<img src="https://i.imgur.com/OxCWm9G.png">
<img src="https://i.imgur.com/p6Wllih.png">
<img src="https://i.imgur.com/e7ba1an.png">
<img src="https://i.imgur.com/4jk3IMz.png">

## Red
<img src="https://i.imgur.com/fFffDXa.png">
<img src="https://i.imgur.com/qjtljLl.png">
<img src="https://i.imgur.com/LvFN1tJ.png">
<img src="https://i.imgur.com/lHCWTWB.png">
<img src="https://i.imgur.com/D9OtGg5.png">

## Teal
<img src="https://i.imgur.com/ZCvFBDS.png">
<img src="https://i.imgur.com/alM4y9g.png">
<img src="https://i.imgur.com/Rzoss82.png">
<img src="https://i.imgur.com/sv73r9p.png">
<img src="https://i.imgur.com/xbPszai.png">

## Violet
<img src="https://i.imgur.com/oxNM7g0.png">
<img src="https://i.imgur.com/IBvwC6a.png">
<img src="https://i.imgur.com/WAjAoVj.png">
<img src="https://i.imgur.com/zJAWWXY.png">
<img src="https://i.imgur.com/oyaqltL.png">

## Yellow
<img src="https://i.imgur.com/bt5lgw5.png">
<img src="https://i.imgur.com/eSDmF3y.png">
<img src="https://i.imgur.com/0q1ETIj.png">
<img src="https://i.imgur.com/7Nb5i9X.png">
<img src="https://i.imgur.com/7ADqqH3.png">
